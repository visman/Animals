#!/bin/bash
# скрипт предназначен только для деплоя на animals-stage. не стоит его использовать где либо еще.

# устанавливаем среду исполнения кода в STAGE
export ENVIRONMENT="STAGE"

# Подготовим окружение для конфига
export DATABASE_NAME="${DATABASE_NAME}"
export DATABASE_USER="${DATABASE_USER}"
export DATABASE_PASSWORD="${DATABASE_PASSWORD}"
export DATABASE_HOST="${DATABASE_HOST}"
export DATABASE_PORT="${DATABASE_PORT}"


export DEPLOY_PATH=/local/karbidsoft/web/animals_stage
export VENV_PATH=/local/karbidsoft/venv/animals_py3_stage
revision=`git describe`

# подготовим вирутальное окружение для проекта
rm -fr ${VENV_PATH}
virtualenv -p python3 ${VENV_PATH}
source ${VENV_PATH}/bin/activate

mkdir -p ${DEPLOY_PATH}/releases ${DEPLOY_PATH}/logs/stage

#копируем
rsync -au --exclude ".git" ./* ${DEPLOY_PATH}/releases/${CI_COMMIT_REF_NAME}

# переходим в папку и делаем всякое
cd ${DEPLOY_PATH}/releases/${CI_COMMIT_REF_NAME}
# установим зависимости проекта
pip3 install --no-cache-dir -r requirements.txt
# очистим базу данных, накатим миграции и создадим супер пользователя
./manage.py flush --noinput
./manage.py makemigrations --noinput
./manage.py makemigrations animan --noinput
./manage.py migrate

#создадим супер пользователя в базе
echo "from django.contrib.auth.models import User; User.objects.create_superuser('${SUPERUSER_LOGIN}', '${SUPERUSER_EMAIL}', '${SUPERUSER_PASSWORD}')" | ./manage.py shell

#debug of delpoy

echo "import os; from Animals.settings import ENV; print('Curreint environment is: {}!!!!'.format(ENV))" | ./manage.py shell
echo "import os; db = os.environ.get('DATABASE_NAME', ''); print('Database is: {}!!!!'.format(db))" | ./manage.py shell
./manage.py collectstatic

# сделаем симлинк на текущую дерикторию
rm ${DEPLOY_PATH}/current
ln -s ${DEPLOY_PATH}/releases/${CI_COMMIT_REF_NAME} ${DEPLOY_PATH}/current
cd ${DEPLOY_PATH}/current

# удалим симлинки на конфиги nginx, supervisord
sudo rm /etc/nginx/sites-enabled/animals-stage
sudo rm /etc/supervisor/conf.d/animals_stage.conf

# добавим конфигурацию nginx
sudo ln -s ${DEPLOY_PATH}/current/conf/nginx/animals_stage /etc/nginx/sites-enabled/animals-stage
# добавим конфигурацию supervisord
sudo ln -s ${DEPLOY_PATH}/current/conf/supervisor/stage.conf /etc/supervisor/conf.d/animals_stage.conf

echo "DATABASE_NAME=${DATABASE_NAME}" > ${DEPLOY_PATH}/current/conf/variables
echo "DATABASE_USER=${DATABASE_USER}" >> ${DEPLOY_PATH}/current/conf/variables
echo "DATABASE_PASSWORD=${DATABASE_PASSWORD}" >> ${DEPLOY_PATH}/current/conf/variables
echo "DATABASE_HOST=${DATABASE_HOST}" >> ${DEPLOY_PATH}/current/conf/variables
echo "DATABASE_PORT=${DATABASE_PORT}" >> ${DEPLOY_PATH}/current/conf/variables

# перезагрузим nginx, supervisord
supervisorctl update
supervisorctl restart all
sudo service nginx reload